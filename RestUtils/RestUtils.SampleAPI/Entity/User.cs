using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using APIUtils.APIUtils;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using RESTUtils.APIUtils;
using RESTUtils.APIUtils.Entity;

namespace RestUtils.SampleAPI.Entity
{
    public class User : BaseEntity, IApplicationUser
    {
        public string email;

        public User(string email)
        {
            this.email = email;
        }

        protected override IList<ValidationResult> PerformValidate(ValidationContext validationContext,
            IdentityDbContext context) =>
            base.PerformValidate(validationContext, context).Check(() =>
                new EmailAddressAttribute().IsValid(email)
                    ? ValidationResult.Success
                    : new ValidationResult("Email invalido", new[] {nameof(User.email)}));
    }
}