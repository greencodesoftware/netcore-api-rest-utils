using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using RestUtils.SampleAPI.Controllers;

namespace RestUtils.SampleAPI
{
    public static class DataExtensions
    {
        public static ApplicationDbContext CastMe(this IdentityDbContext context)
        {
            return (ApplicationDbContext) context;
        }
    }
}