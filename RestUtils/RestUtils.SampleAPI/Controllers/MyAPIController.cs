using System.Linq;
using APIUtils.APIUtils;
using AutoMapper;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using RESTUtils.APIUtils;
using RESTUtils.APIUtils.Entity;

namespace RestUtils.SampleAPI.Controllers
{
    public abstract class MyAPIController<T>: RestController<T> where T:BaseEntity
    {
        public MyAPIController(IdentityDbContext context, IMapper mapper) : base(context, mapper)
        {
        }

        protected override IApplicationUser FindUserByMail(string userMail)
        {
            return ctx().User.Single(user => user.email.Equals(userMail));
        }

        protected ApplicationDbContext ctx()
        {
            return Context.CastMe();
        } 
    }
}