﻿using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using RestUtils.SampleAPI.Entity;

namespace RestUtils.SampleAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : MyAPIController<User>
    {
        

        public UserController(IdentityDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
        
        
    }
}