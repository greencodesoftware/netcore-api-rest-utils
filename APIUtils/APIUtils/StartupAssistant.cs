using System;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.Swagger;

namespace APIUtils.APIUtils
{
    public abstract class StartupAssistant
    {
        public IConfigurationBuilder createBuilder(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddJsonFile("appsettings.private.json", optional: true)
                .AddEnvironmentVariables();
            return builder;
        }

        public void AddJWTAuthentication(IServiceCollection services, IConfiguration configuration)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = configuration["Issuer"],
                        ValidAudience = configuration["Issuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["SecretKey"]))
                    };
                });
        }

        public void AddNpgsql(IServiceCollection services, bool isOnUnitTesting, IConfiguration configuration)
        {
            services.AddEntityFrameworkNpgsql()
                .AddDbContext<IdentityDbContext>(opt =>
                    opt.UseNpgsql(
                        configuration["DefaultConnection"].Replace("Database=",
                            isOnUnitTesting ? "Database=testing_" : "Database="),
                        o => o.CommandTimeout(Convert.ToInt32(TimeSpan.FromMinutes(3).TotalSeconds)))
                );

            //migramos automaticamente la db
            if (isOnUnitTesting)
            {
                var dbContext =
                    (IdentityDbContext) services.BuildServiceProvider().GetService(typeof(IdentityDbContext));
                dbContext.Database.EnsureDeleted();
                dbContext.Database.Migrate();
            }
        }

        public void AddSwagger(IServiceCollection services, string name, string title, string version)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(name, new Info {Title = title, Version = version});
                //c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "CompulsandoApi.xml"));
            });
        }
    }
}