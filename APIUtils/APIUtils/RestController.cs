﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Kendo.DynamicLinq;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Serialization;
using RESTUtils.APIUtils.Entity;

namespace APIUtils.APIUtils
{
    public abstract class RestController<T> : RestController<T, T> where T : BaseEntity
    {
        public RestController(IdentityDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }

    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public abstract class RestController<T, TWrite> : ControllerBase
        where T : BaseEntity
        where TWrite : BaseEntity
    {
        protected IdentityDbContext Context { get; set; }
        protected IMapper Mapper { get; }
        protected Lazy<IApplicationUser> CurrentAppUser;

        public RestController(IdentityDbContext context, IMapper mapper)
        {
            Context = context;
            Mapper = mapper;
            CurrentAppUser = new Lazy<IApplicationUser>(GetAppUser);
        }

        private IApplicationUser GetAppUser()
        {
            var userMail = User?.Identity?.IsAuthenticated != true
                ? throw new InvalidOperationException("not logged in")
                : User.Claims.ToDictionary(c => c.Type, c => c.Value)["http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name"];
            return FindUserByMail(userMail);
        }

        protected abstract IApplicationUser FindUserByMail(string userMail);

        protected static object GetSimpleWsErrorDto(string msg)
        {
            return new { errors = new { error = new[] { msg } } };
        }

        [HttpPost("Search")]
        public DataSourceResult Search([FromBody]KendoGridSearchRequestExtensions.KendoGridSearchRequest request)
        {
            return GetSearchDataSourceResult(request);
        }

        protected virtual DataSourceResult GetSearchDataSourceResult(KendoGridSearchRequestExtensions.KendoGridSearchRequest request)
        {
            var searchDataSourceResult = GetSearchQueryable().ToDataSourceResult(request);
            foreach (var o in searchDataSourceResult.Data)
            {
                CleanFields(o);
            }
            return searchDataSourceResult;
        }

        protected virtual IQueryable<T> GetSearchQueryable()
        {
            return GetQueryableWithIncludes();
        }

        protected virtual IQueryable<T> GetQueryableWithIncludes()
        {
            return Context.Set<T>();
        }

        [HttpGet("GetById/{id}")]
        public async Task<ActionResult<T>> GetById(long id)
        {
            var x = await GetByIdCleaned(id);
            if (x == null) return NotFound();
            return Ok(x);
        }

        protected async Task<T> GetByIdCleaned(long id)
        {
            var x = await GetQueryableWithIncludes().SingleOrDefaultAsync(o => o.Id == id);
            if (x != null) CleanFields(x);
            return x;
        }

        protected virtual void CleanFields(object x)
        {
        }

        [HttpPut("Put/{id}")]
        public virtual async Task<IActionResult> Put(long id, TWrite x)
        {
            if (id != x.Id) return BadRequest();
            var xAux = x is T entity ? entity : Mapper.Map<T>(x);
            return await PerformUpdate(id, xAux);
        }

        protected virtual async Task<IActionResult> PerformUpdate(long id, T xAux)
        {
            TryValidateModel(xAux);
            if (!ModelState.IsValid) return ActionResultForModelStateValidation();
            Upsert(xAux);
            try
            {
                await Context.SaveChangesAsync();
                return NoContent();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Exists(id))
                {
                    return NotFound();
                }
                throw;
            }
        }

        private void Upsert(T entity)
        {
            Context.ChangeTracker.TrackGraph(entity, e =>
            {
                e.Entry.State = e.Entry.IsKeySet
                    ? e.Entry.Entity is BaseEntity o && (o.ShouldDelete ?? false)
                        ? EntityState.Deleted
                        : EntityState.Modified
                    : EntityState.Added;
            });

#if DEBUG
            foreach (var entry in Context.ChangeTracker.Entries())
            {
                Debug.WriteLine($"Entity: {entry.Entity.GetType().Name} State: {entry.State.ToString()}");
            }
#endif
        }

        [HttpPost("Post")]
        public virtual async Task<IActionResult> Post(TWrite x)
        {
            var xAux = x is T entity ? entity : Mapper.Map<T>(x);
            TryValidateModel(xAux);
            if (ModelState.IsValid)
            {
                Context.Add(xAux);
                await Context.SaveChangesAsync();
                xAux = GetQueryableWithIncludes().Single(o => o.Id == xAux.Id);
                return CreatedAtAction(nameof(GetById), new { id = xAux.Id },
                    xAux is TWrite oAux ? oAux : Mapper.Map<TWrite>(xAux));
            }
            return ActionResultForModelStateValidation();
        }

        protected IActionResult ActionResultForModelStateValidation()
        {
            var namingConvention = new CamelCaseNamingStrategy();
            var dict = ModelState.ToDictionary(
                kvp => namingConvention.GetPropertyName(kvp.Key, false),
                kvp => kvp.Value.Errors.Select(e => e.ErrorMessage)
            );
            Response.StatusCode = 400;
            Response.ContentType = "application/json";
            return new JsonResult(new
            {
                Errors = dict
            });
        }

        [HttpDelete("Delete/{id}")]
        public virtual async Task<ActionResult<T>> Delete(long id)
        {
            var x = await Context.FindAsync<T>(id);
            if (x == null)
            {
                return NotFound();
            }

            Context.Remove(x);
            await Context.SaveChangesAsync();

            return x;
        }

        private bool Exists(long id)
        {
            return Context.Set<T>().Any(e => e.Id == id);
        }

    }

    public interface IApplicationUser
    {
        
    }
}