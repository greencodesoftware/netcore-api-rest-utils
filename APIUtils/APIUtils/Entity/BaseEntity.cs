﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace RESTUtils.APIUtils.Entity
{
    public static class ValidationExtensions
    {
        public static IList<ValidationResult> AddValidation(this IList<ValidationResult> v, string msg, IEnumerable<string> memberNames = null)
        {
            v.Add(new ValidationResult(msg, memberNames));
            return v;
        }
        public static IList<ValidationResult> CheckUnique<T>(this IList<ValidationResult> v,
            T x, DbContext context, Expression<Func<T, bool>> compare, string msg = null, IEnumerable<string> memberNames = null)
         where T : BaseEntity
        {
            if (context.Set<T>().Where(o => o.Id != x.Id).Any(compare))
            {
                v.Add(new ValidationResult(msg ?? $"Unicidad no respetada para entidad de tipo {typeof(T).Name}", memberNames));
            }
            return v;
        }

        public static IList<ValidationResult> Check(this IList<ValidationResult> v, Func<ValidationResult> validator)
        {
            var result = validator.Invoke();
            if (result != null && result != ValidationResult.Success) v.Add(result);
            return v;
        }
    }

    public abstract class BaseEntity : IValidatableObject
    {
        public long Id { get; set; }
        [NotMapped]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? ShouldDelete { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var ctx = validationContext.GetService(typeof(IdentityDbContext)) as IdentityDbContext;
            return PerformValidate(validationContext, ctx);
        }

        protected virtual IList<ValidationResult> PerformValidate(ValidationContext validationContext,
            IdentityDbContext context)
        {
            return new List<ValidationResult>();
        }
    }
    public sealed class FkCheck : ValidationAttribute
    {
        public Type TypeToCheck { get; set; }

        protected override ValidationResult IsValid(object entityId, ValidationContext validationContext)
        {
            var ctx = validationContext.GetService(typeof(IdentityDbContext)) as IdentityDbContext;
            return entityId == null
                ||
                (ctx.Find(TypeToCheck, long.TryParse(entityId.ToString(), out var id) ? id : entityId) != null)
                ? ValidationResult.Success
                : new ValidationResult($"No existe el valor elegido!");
        }
    }
    public class MustHaveOneAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            return value is IEnumerable collection && collection.GetEnumerator().MoveNext()
                ? ValidationResult.Success
                : new ValidationResult($"Al menos debe tener un elemento!");
        }
    }
}