﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;


namespace RESTUtils.TestUtils
{
    public interface IWebFixture: IDisposable
    {
        void UpsertUserToken(string email, string token);
        bool checkRegistered(string email);
        Task<JObject> Send<TControllerType>(string action, bool shouldSucceed = true,
            object routeValues = null, object bodyData = null, HttpMethod method = null, IFormFile file = null)
            where TControllerType : ControllerBase;

        Task<T> Send<T, TControllerType>(string action, bool shouldSucceed = true,
            object routeValues = null, object bodyData = null, HttpMethod method = null)
            where TControllerType : ControllerBase;

    }
    public abstract class WebAppFixture<TStartup> : IWebFixture where TStartup : class
    {
        private CustomWebApplicationFactory<TStartup> Factory { get; set; }
        public LinkGenerator LinkGenerator { get; }
        private Dictionary<string, string> CurrentTokens { get; }
        public string CurrentUserEmail { get; set; }

        protected WebAppFixture()
        {
            Factory = new CustomWebApplicationFactory<TStartup>();
            Factory.CreateClient();
            LinkGenerator = (LinkGenerator) Factory.Server.Host.Services.GetService(
                typeof(LinkGenerator));
            CurrentTokens = new Dictionary<string, string>();
        }

        public void Dispose()
        {
            Factory.Dispose();
        }

        public void UpsertUserToken(string email, string token)
        {
            CurrentTokens[email] = token;
        }

        public bool checkRegistered(string email)
        {
            return CurrentTokens.ContainsKey(email);
        }

        public async Task<JObject> Send<TControllerType>(string action, bool shouldSucceed = true,
            object routeValues = null, object bodyData = null, HttpMethod method = null, IFormFile file = null)
            where TControllerType : ControllerBase
        {
            method = method ?? HttpMethod.Post;
            var url = LinkGenerator.GetPathByAction(action, typeof(TControllerType).Name.Replace("Controller", ""),
                routeValues);
            var m = new HttpRequestMessage(method, url);
            if (bodyData != null)
            {
                m.Content = new StringContent(JsonConvert.SerializeObject(bodyData, SerializerSettings()),
                    Encoding.UTF8, "application/json");
            }

            if (file != null)
            {
                var multiContent = new MultipartFormDataContent();
                var fileStreamContent = new StreamContent(file.OpenReadStream());
                multiContent.Add(fileStreamContent, "file", file.FileName);
                m.Content = multiContent;
            }

            if (!string.IsNullOrWhiteSpace(CurrentUserEmail))
            {
                m.Headers.Add("Authorization", $"Bearer {CurrentTokens[CurrentUserEmail]}");
            }

            var httpResponse = await Factory.CreateDefaultClient().SendAsync(m);
            var json = await httpResponse.Content.ReadAsStringAsync();
            if (String.IsNullOrEmpty(json))
            {
                var responseStream = await httpResponse.Content.ReadAsStreamAsync();
                return JObject.Parse(responseStream.ToString());
            }
            
            if (shouldSucceed)
            {
                Debug.WriteLine(json);
                httpResponse.EnsureSuccessStatusCode();
            }
            else
            {
                var statusCode = (int) httpResponse.StatusCode;
                if (statusCode >= 200 && statusCode < 300)
                {
                    throw new InvalidOperationException("Should not be success!");
                }
            }

            return JObject.Parse(json);
        }

        public async Task<T> Send<T, TControllerType>(string action, bool shouldSucceed = true,
            object routeValues = null, object bodyData = null, HttpMethod method = null)
            where TControllerType : ControllerBase
        {
            var jobject = await Send<TControllerType>(action, shouldSucceed, routeValues, bodyData, method);
            return jobject.ToObject<T>(JsonSerializer.Create(SerializerSettings()));
        }

        private static JsonSerializerSettings SerializerSettings()
        {
            return new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };
        }
    }
}