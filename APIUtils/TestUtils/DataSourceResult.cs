﻿using System.Collections.Generic;

namespace Tests
{
    public class DataSourceResult<T>
    {
        public List<T> Data { get; set; }
    }
}