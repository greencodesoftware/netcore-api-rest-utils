﻿using System.Collections.Generic;
using System.Net.Http;
using Kendo.DynamicLinq;
using Microsoft.AspNetCore.Mvc;
using RESTUtils.APIUtils.Entity;
using Tests;

namespace RESTUtils.TestUtils
{
    public static class WebAppWsHelpers
    {
        public static T GetById<T, TControllerType>(this IWebFixture fixture, long id) where TControllerType : ControllerBase
        {
            return fixture.Send<T, TControllerType>("GetById", routeValues: new { id }, method: HttpMethod.Get).Result;
        }

        public static List<T> Search<T, TControllerType>(
            this IWebFixture fixture,
            KendoGridSearchRequestExtensions.KendoGridSearchRequest request = null
        ) where TControllerType : ControllerBase
        {
            return fixture.Send<DataSourceResult<T>, TControllerType>("Search", bodyData: (object)request ?? new { take = 10 }).Result.Data;
        }

        public static List<T> SearchByAttr<T, TControllerType>(
            this IWebFixture fixture,
            string attrName, string attrValue
        ) where TControllerType : ControllerBase
        {
            var bodyData = new KendoGridSearchRequestExtensions.KendoGridSearchRequest
            {
                take = 10,
                filter = new Filter
                {
                    Logic = "and",
                    Filters = new List<Filter>
                    {
                        new Filter
                        {
                            Field = attrName,
                            Operator = "eq",
                            Value = attrValue
                        }
                    }
                }
            };
            return fixture.Send<DataSourceResult<T>, TControllerType>("Search", bodyData: bodyData).Result.Data;
        }
    }
}